# Progress
* [x] 1. Getting Started
* [x] 2. Programming a Guessing Game
* [x] 3. Common Programming Concepts
* [x] 4. Understanding Ownership
* [x] 5. Using Structs to Structure Related Data
* [x] 6. Enums and Pattern Matching
* [x] 7. Managing Growing Projects with Packages, Crates, and Modules
* [x] 8. Common Collections
* [x] 9. Error Handling
* [x] 10. Generic Types, Traits, and Lifetimes
* [x] 11. Writing Automated Tests
* [x] 12. An I/O Project: Building a Command Line Program
* [x] 13. Functional Language Features: Iterators and Closures
* [x] 14. More about Cargo and Crates.io
* [x] 15. Smart Pointers
* [x] 16. Fearless Concurrency
* [x] 17. Object Oriented Programming Features of Rust
* [x] 18. Patterns and Matching
* [x] 19. Advanced Features (split into subs because big)
  * [x] 19.1 unsafe rust
  * [x] 19.2 Advanced traits
  * [x] 19.3 Advanced Types
  * [x] 19.4 Advanced Functions and Closures
  * [x] 19.5 Macros
* [x] 20. Final Project: Building a Multithreaded Web Server
  * [x] 20.1. Building a Single-Threaded Web Server
  * [x] 20.2. Turning Our Single-Threaded Server into a Multithreaded Server
  * [x] 20.3. Graceful Shutdown and Cleanup
