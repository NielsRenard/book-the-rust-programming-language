use hello_macro::HelloMacro;
use hello_macro_derive::HelloMacro;

#[derive(HelloMacro)]
struct Pancakes;

#[derive(HelloMacro)]
struct Flans;

fn main() {
    Pancakes::hello_macro();
    Flans::hello_macro();
}
