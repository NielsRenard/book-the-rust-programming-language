mod advanced_traits;
use advanced_traits::*;

fn main() {
    let point = Point { x: 100, y: 2 };
    point.outline_print();
}
