use crate::List::{Cons, Nil};
use std::rc::Rc;

fn main() {
    box_type();
    println!("");
    cons_list();
    println!("");
    deref_operator();
    println!("");
    deref_coercion();
    println!("");
    drop_with_printlines();
    println!("");
    force_drop();
    println!("");
    reference_counted_cons_list();
    println!("");
}

fn box_type() {
    let b = Box::new(5);
    println!("b = {}", b);
}

// This code will produce the error below.
//
// enum List {
//     Cons(i32, List),
//     Nil,
// }
//
//  rustc: recursive type `List` has infinite size recursive without
//  indirection help: insert indirection (e.g., a `Box`, `Rc`, or `&`)
//  at some point to make `List` representable
//

#[derive(Debug)]
enum List {
    Cons(i32, Box<List>),
    Nil,
}

fn cons_list() {
    let list = Box::new(Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil)))))));
    println!("cons list = {:?}", list);
}

fn deref_operator() {
    let x = 5;
    let y = &x;

    assert_eq!(5, x);

    // assert_eq!(5, y);
    // this assert will give the following error.
    //
    // rustc: can't compare `{integer}` with `&{integer}`
    // no implementation for `{integer} == &{integer}`
    //
    // We must dereference the pointer, as such:
    assert_eq!(5, *y);
}

fn _deref_with_box() {
    let x = 5;
    let y = Box::new(x);

    assert_eq!(5, x);
    // dereffing a Box<T> with * works because you can treat Box<T>
    // like a reference
    assert_eq!(5, *y);
}

fn deref_coercion() {
    let m = Box::new(String::from("World"));
    // with:
    println!("Hello, {}!", m);
    // versus without
    println!("Hello, {}!", &(*m)[..]);
}

fn drop_with_printlines() {
    struct CustomSmartPointer {
        data: String,
    }

    impl Drop for CustomSmartPointer {
        fn drop(&mut self) {
            println!("Dropping CustomSmartPointer with data `{}`!", self.data);
        }
    }
    let _c = CustomSmartPointer {
        data: String::from("first smart pointer"),
    };
    println!("first smart pointer created.");

    // using a scope to force a shorter lifetime
    {
        let _x = CustomSmartPointer {
            data: String::from("second smart pointer (short scope)"),
        };
        println!("second smart pointer created (short scope).");
    }

    let _d = CustomSmartPointer {
        data: String::from("third smart pointer"),
    };

    println!("third smart pointer created.");
}

fn force_drop() {
    struct CustomSmartPointer {
        data: String,
    }

    impl Drop for CustomSmartPointer {
        fn drop(&mut self) {
            println!("Dropping CustomSmartPointer with data `{}`!", self.data);
        }
    }
    let c = CustomSmartPointer {
        data: String::from("first smart pointer"),
    };
    println!("first smart pointer created.");

    let _d = CustomSmartPointer {
        data: String::from("second smart pointer"),
    };

    println!("second smart pointer created.");

    // force drop first smart pointer before second
    drop(c);
}

fn reference_counted_cons_list() {
    // Bit clunky because of similarity to List Enum. TODO: make that
    // one local as well.
    enum RcList {
        Cons(i32, Rc<RcList>),
        Nil,
    }
    let a = Rc::new(RcList::Cons(
        5,
        Rc::new(RcList::Cons(10, Rc::new(RcList::Nil))),
    ));
    println!("count after creating a = {}", Rc::strong_count(&a));

    // scope to shorten lifetime
    {
        let _b = RcList::Cons(3, Rc::clone(&a));
        println!("count after creating b = {}", Rc::strong_count(&a));
    }

    println!("count after b goes out of scope = {}", Rc::strong_count(&a));
    let _c = RcList::Cons(4, Rc::clone(&a));
    println!("count after creating c = {}", Rc::strong_count(&a));
}
