mod blog;
use blog::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch today");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    // //will work once third extra feature is implemented
    // //this will not get added because add_text only works in Draft state
    //post.add_text(", and a cheeseburger for dinner.");

    post.reject();
    assert_eq!("", post.content());

    // //will work once third extra feature is implemented
    // post.add_text(", and a hamburger for dinner.");
    // assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.approve();
    // only after two approves does a Post become Published
    assert_eq!("", post.content());

    post.approve();
    assert_eq!("I ate a salad for lunch today", post.content());

    // //will work once third extra feature is implemented
    //assert_eq!("I ate a salad for lunch today, and a hamburger for dinner.", post.content());
}
