use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

fn main() {
    //    println!("basic");
    //    _basic();
    // println!("multiple messages, waiting");
    // _multiple_messages();
    // println!("multiple channels, waiting");
    // _multiple_channels();

    println!("mutex:");
    _mutex_multiple_threads();
}

fn _basic() {
    println!("Hello, world!");

    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let val = "Hi";
        let val_2 = "HI";
        tx.send(val).unwrap();
        tx.send(val_2).unwrap();
    });

    let received = rx.recv().unwrap();
    println!("Got: {}", received);

    let received_2 = rx.recv().unwrap();
    println!("Also got: {}", received_2);
}

fn _multiple_messages() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(400));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}

fn _multiple_producers() {
    let (tx, rx) = mpsc::channel();

    let tx_clone = mpsc::Sender::clone(&tx);

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx_clone.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx {
        println!("Got: {}", received);
    }
}

fn _multiple_channels() {
    let (tx, rx) = mpsc::channel();
    let (tx2, rx2) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
        // drop the Sender after it sends all vals
        drop(tx);
    });

    let handle1_5 = thread::spawn(move || {
        for received in rx {
            println!("Got: {}, passing it on", received);
            tx2.send(received).unwrap();
        }
        // this thread dies since rx detects tx was dropped
    });

    handle1_5.join().unwrap();

    let handle2 = thread::spawn(move || {
        for received in rx2 {
            println!("Got: {}", received);
            thread::sleep(Duration::from_secs(1));
        }
        // this thread dies because handle1_5 dies, closing tx2
    });

    // main thread waits until handle2 finishes
    handle2.join().unwrap();
}

fn _mutex() {
    let m = Mutex::new(5);

    {
        let mut num = m.lock().unwrap();
        *num = 6;
    }

    println!("m = {:?}", m); // m = Mutex { data: 6 }
}

fn _mutex_multiple_threads() {
   let counter = Arc::new(Mutex::new(0));
   let mut handles = vec![];

   for _ in 0..10 {
       let counter_clone = Arc::clone(&counter);
       let handle = thread::spawn(move || {
           let mut num = counter_clone.lock().unwrap();

           *num += 1;
       });
       handles.push(handle);
   }

   for handle in handles {
       handle.join().unwrap();
   }

   println!("Result: {}", *counter.lock().unwrap());
}
