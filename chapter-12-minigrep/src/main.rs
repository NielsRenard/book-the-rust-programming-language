use std::env;
use std::process;
mod lib;
use lib::Config;

fn main() {
    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    let result = lib::run(config);

    match result {
        Err(e) => {
            eprintln!("Application error: {}", e);
            process::exit(1);
        }
        // not great, but the alternative is an `if let`
        Ok(()) => (),
    }
}
