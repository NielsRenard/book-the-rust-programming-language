#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
    z: (i32, i32, i32),
}

fn main() {
    let p = Point {
        x: 0,
        y: 7,
        z: (1, 2, 3),
    };

    let Point { x: a, y: b, z: _ } = p; // names don't _have to_ match variable names from the struct

    #[allow(non_shorthand_field_patterns)]
    let Point {
        x: x,
        y,
        z: (first, .., last),
    } = p; // but they can if you want (this will give a non_shorthand_field_patterns warning)
    assert_eq!(0, a);
    assert_eq!(7, b);
    assert_eq!(1, first);
    assert_eq!(3, last);
    assert_eq!(0, x);
    assert_eq!(7, y);

    println!(
        "p {:?}\na {:?}, b {:?}, first: {}, last: {}",
        p, a, b, first, last
    );
    println!("p {:?}\nx {:?}, y {:?}", p, x, y);

    let Point { x, y, .. } = p; // you can also use this shorthand to have matching var names
    println!("p {:?}\nx {:?}, y {:?}", p, x, y);

    match_guard();

    destructuring_variable_assignment();
}

fn match_guard() {
    let num = Some(4);
    let y = 5;
    match num {
        Some(x) if x < y => println!("less than five: {}", x), // match guards don't bind (or shadow) new variables (this y is the outer y)
        Some(x) => println!("{}", x),
        None => (),
    }
}

enum Message {
    Hello { id: i32 },
}

// Using @ lets us test a value and save it in a variable within one pattern.
fn destructuring_variable_assignment() {
    let msg = Message::Hello { id: 5 };

    match msg {
        Message::Hello {
            id: id_variable @ 3..=7,
        } => println!("Found an id in range: {}", id_variable), // <----------------- handy because we can't access the id field otherwise
        Message::Hello { id: 10..=12 } => println!("Found an id in another range"),
        Message::Hello { id } => println!("Found some other id: {}", id), //<----- can access id, but can't test
    }
}
