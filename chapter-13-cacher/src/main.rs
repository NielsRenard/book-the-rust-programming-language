use core::hash::Hash;
use std::collections::HashMap;
use std::{thread, time::Duration, time::SystemTime};

struct Cacher<T, U>
where
    T: Fn(U) -> U,
{
    calculation: T,
    value: HashMap<U, Option<U>>,
}

impl<T, U> Cacher<T, U>
where
    T: Fn(U) -> U,
    U: Hash + Eq + Copy,
{
    fn new(calculation: T) -> Cacher<T, U> {
        Cacher {
            calculation,
            value: HashMap::new(),
        }
    }

    fn value(&mut self, arg: U) -> U {
        match self.value.get(&arg) {
            Some(v) => v.unwrap(),
            None => {
                let v = (self.calculation)(arg);
                self.value.insert(arg, Some(v));
                v
            }
        }
    }
}

fn main() {
    // use the Cacher with int's
    generate_workout(10, 5);
    // use the Cacher with &str slices
    longer("short", "longer");
}

fn longer(a: &str, b: &str) {
    let mut cacher = Cacher::new(|num| {
        thread::sleep(Duration::from_secs(1));
        num
    });

    if a.len() > b.len() {
        cacher.value(a);
    } else {
        cacher.value(b);
    }

    println!("longer cacher: {:?}", cacher.value);
}

fn generate_workout(intensity: u32, random_number: u32) {
    let mut expensive_result = Cacher::new(|num| {
        thread::sleep(Duration::from_secs(1));
        num
    });

    if intensity < 25 {
        println!("tick 1 {:?}", SystemTime::now());
        println!("Today, do {} pushups!", expensive_result.value(intensity));
        println!("tock 1 {:?}", SystemTime::now());

        println!("tick 2 {:?}", SystemTime::now());
        println!(
            "Next, do {} situps!",
            // using a different 'insentity' here, to test new HashMap implementation
            expensive_result.value(100)
        );
        println!("tock 2 {:?}", SystemTime::now());
    } else {
        if random_number == 3 {
            println!("Take a break today! Remember to stay hydrated!");
        } else {
            println!(
                "Today, run for {} minutes!",
                expensive_result.value(intensity)
            );
        }
    }

    println!(
        "generate_workout expensive_result: {:?}",
        expensive_result.value
    );
}
